(ns yami.core)

(defonce captured (atom {}))

(defmacro capture!
  [funcall-form]
  (let [[f & [arg :as args]] funcall-form
        f-var (when (symbol? f) (resolve f))]
    `(let [f# ~f
           args# (mapv eval '~args)
           ret# (apply f# args#)]
       (reset! captured (update @captured f# #(conj % {:f f#
                                                       :quoted-f '~f
                                                       :f-var ~f-var
                                                       :args args#
                                                       :ret ret#})))
       ret#)))

(defn report!
  []
  (let [bar "=========================="
        var-to-str (fn [var]
                     (let [{:keys [ns name]} (meta var)]
                       (str ns "/" name)))]
    (println "@@@@@ report @@@@@")
    (doseq [[f [{:keys [f-var]} :as exs]] @captured
            :let [fname (if f-var
                          (var-to-str f-var)
                          (str f))]]
      (println bar)
      (println fname "examples")
      (doseq [{:keys [f args ret]} exs]
        (apply print (str "(" fname) args)
        (println ")")
        (println "=>" (pr-str ret)))
      (println bar))
    (println "@@@ end report @@@")))

(defn reset-captured!
  []
  (reset! captured {}))

(comment
  (defn hoge
    [x]
    "hoge")
  (gensym "args")
  (reset! captured {})
  (report!)
  (mapv #(vector %1 %2)
        (repeatedly #(gensym "arg"))
        '(1 2))
  (eval '(identity 1))
  (eval '(hoge 1))
  (capture!
   (identity 1))
  (capture!
   (identity 2))
  (capture!
   ((fn [x] x) 1))
  (capture!
   (identity (+ 1 1)))
  (capture!
   (identity (inc 1)))
  (capture!
   (identity (inc 1)))
  (capture!
   (+ 1 2 3))
  (capture!
   (hoge 10))
  (+ 1 2)
  (meta #'+)
  (meta #'report!) ;; fails. why???
  (-> (@captured identity)
      first
      :quoted-f
      resolve
      type)
  (-> (resolve 'clojure.test/is)
      meta
      :name)
  (-> (fn [x] x)
      resolve))
