(ns simple
  (:require [yami.core :as yami]))

(defn f
  [x]
  (+ (* 2 x) 1))

(yami/capture!
 (f 1))

(yami/capture!
 (f (+ 1 1)))

(yami/report)

(comment
  (yami/reset-captured!))
