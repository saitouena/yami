# yami

[![Clojars Project](https://img.shields.io/clojars/v/yami.svg)](https://clojars.org/yami)

Simple debugging aid for Clojure

## Installation
Add the following to your project dev dependencies or the :user profile in ~/.lein/profiles.clj:


[![Clojars Project](https://clojars.org/yami/latest-version.svg)](https://clojars.org/yami)


## Usage

There is two main api: `capture!` and `report!`.
`capture!` captures arguments and the result of the function call.
`report` reports all `capture!`-d results.

```clojure
(ns simple
  (:require [yami.core :as yami])) ;; require yami.core

(defn f
  [x]
  (+ (* 2 x) 1))

(yami/capture!
 (f 1)) ;; capture function call

(yami/capture!
 (f (+ 1 1))) ;; capture function call

(yami/report)
```
output to REPL:
```
@@@@@ report @@@@@
==========================
simple/f examples
(simple/f 2)
=> 5
(simple/f 1)
=> 3
==========================
@@@ end report @@@
```

- You can reset all captured results with `yami/reset-captured!`.
- You can access raw captured results through `@yami/captured`.

## TODO
see TODO.org

## License

Copyright ��� saitouena 2020

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
